<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use app\models\User;
use app\models\Project;
use app\models\Status;

/**
 * This is the model class for table "task".
 *
 * @property integer $taskId
 * @property integer $statusId
 * @property integer $projectId
 * @property string $title
 * @property string $body
 * @property string $requiredFD
 * @property string $actualFD
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statusId', 'projectId'], 'required'],
            [['statusId', 'projectId'], 'integer'],
            [['body'], 'string'],
            [['requiredFD', 'actualFD', 'created_at', 'updated_at'], 'safe'],
            [['title', 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'taskId' => 'Task ID',
            'statusId' => 'Status ID',
            'projectId' => 'Project ID',
            'title' => 'Title',
            'body' => 'Body',
            'requiredFD' => 'Required Finish Date',
            'actualFD' => 'Actual Finish Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
	
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at',],
				],
			],
		];
    }

	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['userId' => 'created_by']);  //[originColumn => newColumn]
    }	

	public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['userId' => 'updated_by']);  //[originColumn => newColumn]
    }
	
	public function getStatusItem()
    {												
        return $this->hasOne(Status::className(), ['statusId' => 'statusId']);  //[originColumn => newColumn]
    }
	
	public function getProjectItem()
    {												
        return $this->hasOne(Project::className(), ['projectId' => 'projectId']);  //[originColumn => newColumn]
    }	
}