<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "status".
 *
 * @property integer $statusId
 * @property string $statusName
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statusName'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'statusId' => 'Status ID',
            'statusName' => 'Status Name',
        ];
    }
	
	public static function getStatuses()
	{
		$allStatuses = self::find()->all();
		$allStatusesArray = ArrayHelper::
					map($allStatuses, 'statusId', 'statusName');
		return $allStatusesArray;						
	}
}
