<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use app\models\User;

/**
 * This is the model class for table "project".
 *
 * @property integer $projectId
 * @property integer $userId
 * @property string $projectName
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId'], 'required'],
            [['userId'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['projectName', 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'projectId' => 'Project ID',
            'userId' => 'User Name',
            'projectName' => 'Project Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
	
	public static function getProjects()
	{
		$allProjects = self::find()->all();
		$allProjectsArray = ArrayHelper::
					map($allProjects, 'projectId', 'projectName');
		return $allProjectsArray;						
	}
	
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }

	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['userId' => 'created_by']);
    }	

	public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['userId' => 'updated_by']);
    }
	
	public function getUserItem()
    {												
        return $this->hasOne(User::className(), ['userId' => 'userId']);  //[originColumn => newColumn]
    }
	
}
