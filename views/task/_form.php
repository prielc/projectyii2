<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;
use app\models\Project;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>

    <?/*= $form->field($model, 'statusId')->textInput() */?>
	<?= $form->field($model, 'statusId')-> dropDownList(Status::getStatuses()) ?> 

    <?/*= $form->field($model, 'projectId')->textInput() */?>
	<?= $form->field($model, 'projectId')-> dropDownList(Project::getProjects()) ?> 

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

    <?/*= $form->field($model, 'requiredFD')->textInput() */?>
	<?= $form->field($model, 'requiredFD')->widget(
		DatePicker::className(), [
			// inline too, not bad
			 'inline' => false, 
			 // modify template for custom rendering
			//'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
			'clientOptions' => [
				'autoclose' => true,
				'format' => 'dd-M-yyyy'
			]
	]); ?>

    <?/*= $form->field($model, 'actualFD')->textInput() */?>
	<?= $form->field($model, 'actualFD')->widget(
		DatePicker::className(), [
			// inline too, not bad
			 'inline' => false, 
			 // modify template for custom rendering
			//'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
			'clientOptions' => [
				'autoclose' => true,
				'format' => 'dd-M-yyyy'
			]
	]); ?>

    <div style="display:none;"> <?= $form->field($model, 'created_at')->textInput() ?> </div>

    <div style="display:none;"> <?= $form->field($model, 'updated_at')->textInput() ?> </div>

    <div style="display:none;"> <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?> </div>

    <div style="display:none;"> <?= $form->field($model, 'updated_by')->textInput(['maxlength' => true]) ?> </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
