<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Task */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->taskId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->taskId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            /*'taskId',*/
            /*'statusId',*/
			[
			'attribute' => 'statusId',
				'label' => 'Status Name',
				'value' => function($model){
					return $model->statusItem->statusName;
				},	
			],
            /*'projectId',*/
			[
			'attribute' => 'projectId',
				'label' => 'Project Name',
				'value' => function($model){
					return $model->projectItem->projectName;
				},	
			],
            'title',
            'body:ntext',
            'requiredFD',
            'actualFD',
            /*'created_at',*/
			[ // task created at
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],
            /*'updated_at',*/
			[ // task updated at
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],
            /*'created_by',*/
			[
			'attribute' => 'created_by',
				'label' => 'Created By',
				'value' => function($model){
					return $model->createdBy->username;
				},	
			],
            /*'updated_by',*/
			[
			'attribute' => 'updated_by',
				'label' => 'Updated By',
				'value' => function($model){
					return $model->updatedBy->username;
				},	
			],
        ],
    ]) ?>

</div>
