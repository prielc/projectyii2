<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?/*= $form->field($model, 'userId')->textInput() */?>
	<?= $form->field($model, 'userId')-> dropDownList(User::getUsers()) ?> 

    <?= $form->field($model, 'projectName')->textInput(['maxlength' => true]) ?>

    <div style="display:none;"> <?= $form->field($model, 'created_at')->textInput() ?> </div>

    <div style="display:none;"> <?= $form->field($model, 'updated_at')->textInput() ?> </div>

    <div style="display:none;"> <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?> </div>

    <div style="display:none;"> <?= $form->field($model, 'updated_by')->textInput(['maxlength' => true]) ?> </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
