<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = $model->projectName;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->projectId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->projectId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            /*'projectId',*/
            /*'userId',*/
			[
			'attribute' => 'userId',
				'label' => 'User Name',
				'value' => function($model){
					return $model->userItem->username;
				},	
			],
            'projectName',
            /*'created_at',
            'updated_at',*/
			[ // task created at
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],
			[ // task updated at
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],
            /*'created_by',*/
			[
			'attribute' => 'created_by',
				'label' => 'Created By',
				'value' => function($model){
					return $model->createdBy->username;
				},	
			],
            /*'updated_by',*/
			[
			'attribute' => 'updated_by',
				'label' => 'Updated By',
				'value' => function($model){
					return $model->updatedBy->username;
				},	
			],
        ],
    ]) ?>

</div>
