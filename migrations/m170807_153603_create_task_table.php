<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 * Has foreign keys to the tables:
 *
 * - `status`
 * - `project`
 */
class m170807_153603_create_task_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task', [
            'taskId' => $this->primaryKey(),
            'statusId' => $this->integer()->notNull(),
            'projectId' => $this->integer()->notNull(),
            'title' => $this->string(),
            'body' => $this->text(),
            'requiredFD' => $this->string(),
            'actualFD' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->string(),
            'updated_by' => $this->string(),
        ]);

        // creates index for column `statusId`
        $this->createIndex(
            'idx-task-statusId',
            'task',
            'statusId'
        );

        // add foreign key for table `status`
        $this->addForeignKey(
            'fk-task-statusId',
            'task',
            'statusId',
            'status',
            'id',
            'CASCADE'
        );

        // creates index for column `projectId`
        $this->createIndex(
            'idx-task-projectId',
            'task',
            'projectId'
        );

        // add foreign key for table `project`
        $this->addForeignKey(
            'fk-task-projectId',
            'task',
            'projectId',
            'project',
            'projectId',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `status`
        $this->dropForeignKey(
            'fk-task-statusId',
            'task'
        );

        // drops index for column `statusId`
        $this->dropIndex(
            'idx-task-statusId',
            'task'
        );

        // drops foreign key for table `project`
        $this->dropForeignKey(
            'fk-task-projectId',
            'task'
        );

        // drops index for column `projectId`
        $this->dropIndex(
            'idx-task-projectId',
            'task'
        );

        $this->dropTable('task');
    }
}
