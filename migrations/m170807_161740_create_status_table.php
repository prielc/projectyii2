<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status`.
 */
class m170807_161740_create_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status', [
            'statusId' => $this->primaryKey(),
            'statusName' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status');
    }
}
