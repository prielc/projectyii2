<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m170807_152437_create_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project', [
            'projectId' => $this->primaryKey(),
            'userId' => $this->integer()->notNull(),
            'projectName' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->string(),
            'updated_by' => $this->string(),
        ]);

        // creates index for column `userId`
        $this->createIndex(
            'idx-project-userId',
            'project',
            'userId'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-project-userId',
            'project',
            'userId',
            'user',
            'userId',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-project-userId',
            'project'
        );

        // drops index for column `userId`
        $this->dropIndex(
            'idx-project-userId',
            'project'
        );

        $this->dropTable('project');
    }
}
