<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project_user`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `project`
 */
class m170807_154032_create_project_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project_user', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer()->notNull(),
            'projectId' => $this->integer()->notNull(),
            'since' => $this->string(),
        ]);

        // creates index for column `userId`
        $this->createIndex(
            'idx-project_user-userId',
            'project_user',
            'userId'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-project_user-userId',
            'project_user',
            'userId',
            'user',
            'userId',
            'CASCADE'
        );

        // creates index for column `projectId`
        $this->createIndex(
            'idx-project_user-projectId',
            'project_user',
            'projectId'
        );

        // add foreign key for table `project`
        $this->addForeignKey(
            'fk-project_user-projectId',
            'project_user',
            'projectId',
            'project',
            'projectId',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-project_user-userId',
            'project_user'
        );

        // drops index for column `userId`
        $this->dropIndex(
            'idx-project_user-userId',
            'project_user'
        );

        // drops foreign key for table `project`
        $this->dropForeignKey(
            'fk-project_user-projectId',
            'project_user'
        );

        // drops index for column `projectId`
        $this->dropIndex(
            'idx-project_user-projectId',
            'project_user'
        );

        $this->dropTable('project_user');
    }
}
