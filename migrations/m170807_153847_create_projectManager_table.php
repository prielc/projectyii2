<?php

use yii\db\Migration;

/**
 * Handles the creation of table `projectManager`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m170807_153847_create_projectManager_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('projectManager', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer()->notNull(),
            'seniority' => $this->integer(),
        ]);

        // creates index for column `userId`
        $this->createIndex(
            'idx-projectManager-userId',
            'projectManager',
            'userId'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-projectManager-userId',
            'projectManager',
            'userId',
            'user',
            'userId',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-projectManager-userId',
            'projectManager'
        );

        // drops index for column `userId`
        $this->dropIndex(
            'idx-projectManager-userId',
            'projectManager'
        );

        $this->dropTable('projectManager');
    }
}
