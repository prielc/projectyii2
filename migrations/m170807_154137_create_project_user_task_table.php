<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project_user_task`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `project`
 * - `task`
 */
class m170807_154137_create_project_user_task_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project_user_task', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer()->notNull(),
            'projectId' => $this->integer()->notNull(),
            'taskId' => $this->integer()->notNull(),
        ]);

        // creates index for column `userId`
        $this->createIndex(
            'idx-project_user_task-userId',
            'project_user_task',
            'userId'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-project_user_task-userId',
            'project_user_task',
            'userId',
            'user',
            'userId',
            'CASCADE'
        );

        // creates index for column `projectId`
        $this->createIndex(
            'idx-project_user_task-projectId',
            'project_user_task',
            'projectId'
        );

        // add foreign key for table `project`
        $this->addForeignKey(
            'fk-project_user_task-projectId',
            'project_user_task',
            'projectId',
            'project',
            'projectId',
            'CASCADE'
        );

        // creates index for column `taskId`
        $this->createIndex(
            'idx-project_user_task-taskId',
            'project_user_task',
            'taskId'
        );

        // add foreign key for table `task`
        $this->addForeignKey(
            'fk-project_user_task-taskId',
            'project_user_task',
            'taskId',
            'task',
            'taskId',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-project_user_task-userId',
            'project_user_task'
        );

        // drops index for column `userId`
        $this->dropIndex(
            'idx-project_user_task-userId',
            'project_user_task'
        );

        // drops foreign key for table `project`
        $this->dropForeignKey(
            'fk-project_user_task-projectId',
            'project_user_task'
        );

        // drops index for column `projectId`
        $this->dropIndex(
            'idx-project_user_task-projectId',
            'project_user_task'
        );

        // drops foreign key for table `task`
        $this->dropForeignKey(
            'fk-project_user_task-taskId',
            'project_user_task'
        );

        // drops index for column `taskId`
        $this->dropIndex(
            'idx-project_user_task-taskId',
            'project_user_task'
        );

        $this->dropTable('project_user_task');
    }
}
