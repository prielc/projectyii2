<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170803_173904_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'userId' => $this->primaryKey(),
            'username' => $this->string(),
            'password' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
